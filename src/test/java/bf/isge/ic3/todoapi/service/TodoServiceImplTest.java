package bf.isge.ic3.todoapi.service;

import bf.isge.ic3.todoapi.dto.CreateTodoDto;
import bf.isge.ic3.todoapi.model.Todo;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

public class TodoServiceImplTest {
    @Test
    public void testCreateTodo(){
        CreateTodoDto createTodoDto = new CreateTodoDto();
        createTodoDto.setTitle("Titre 1");
        createTodoDto.setDescription("Description 1");
        createTodoDto.setAuthorId(1);

        TodoService todoService = new TodoServiceImpl(new UserServiceImpl());
        Todo created = todoService.createTodo(createTodoDto);
        Assertions.assertThat(created).isNotNull();
        Assertions.assertThat(created.getId()).isNotNull();
        Assertions.assertThat(created.getTitle()).isEqualTo("Titre 1");
        Assertions.assertThat(created.getDescription()).isEqualTo("Description 1");
        Assertions.assertThat(created.getAuthor()).isNull();
    }

    @Test
    public void shouldFindTodoWhenCreated() {
        TodoService todoService = new TodoServiceImpl(new UserServiceImpl());
        CreateTodoDto createTodoDto1 = new CreateTodoDto();
        createTodoDto1.setTitle("Titre 1");
        Todo todo1 = todoService.createTodo(createTodoDto1);

        CreateTodoDto createTodoDto2 = new CreateTodoDto();
        createTodoDto2.setTitle("Titre 2");
        Todo todo2 = todoService.createTodo(createTodoDto2);

        Assertions.assertThat(todoService.findById(todo1.getId())).isNotEmpty();
        Assertions.assertThat(todoService.findById(todo2.getId())).isNotEmpty();

    }

    @Test
    public void shouldNotFindTodoWhenNotCreated() {
        TodoService todoService = new TodoServiceImpl(new UserServiceImpl());
        Assertions.assertThat(todoService.findById(10)).isEmpty();
    }

    @Test
    public void shouldFind2TodoWhen2TodoCreated() {
        TodoService todoService = new TodoServiceImpl(new UserServiceImpl());
        CreateTodoDto createTodoDto1 = new CreateTodoDto();
        createTodoDto1.setTitle("Titre 1");
        todoService.createTodo(createTodoDto1);

        CreateTodoDto createTodoDto2 = new CreateTodoDto();
        createTodoDto2.setTitle("Titre 2");
        todoService.createTodo(createTodoDto2);

        Assertions.assertThat(todoService.findAll().size()).isEqualTo(2);

    }

    @Test
    public void testRemove() {
        TodoService todoService = new TodoServiceImpl(new UserServiceImpl());
        CreateTodoDto createTodoDto1 = new CreateTodoDto();
        createTodoDto1.setTitle("Titre 1");
        Todo todo1 = todoService.createTodo(createTodoDto1);

        CreateTodoDto createTodoDto2 = new CreateTodoDto();
        createTodoDto2.setTitle("Titre 2");
        todoService.createTodo(createTodoDto2);

        Assertions.assertThat(todoService.findAll().size()).isEqualTo(2);

        boolean removed = todoService.removeTodo(todo1.getId());
        Assertions.assertThat(todoService.findAll().size()).isEqualTo(1);
        Assertions.assertThat(removed).isTrue();

        boolean removedNotExisting = todoService.removeTodo(10);
        Assertions.assertThat(removedNotExisting).isFalse();
    }

}
