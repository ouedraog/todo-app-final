package bf.isge.ic3.todoapi.controller.web;

import bf.isge.ic3.todoapi.dto.CreateTodoDto;
import bf.isge.ic3.todoapi.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class TodoPageController {
    @Value("${error.message}")
    private String errorMessage;

    @Autowired
    private TodoService todoService;

    @RequestMapping(value = { "/todoList" }, method = RequestMethod.GET)
    public String todoList(Model model) {
        model.addAttribute("todoList", todoService.findAll());
        return "todoList";
    }

    @RequestMapping(value = { "/addTodo" }, method = RequestMethod.GET)
    public String showAddTodoPage(Model model) {
        CreateTodoDto todoForm = new CreateTodoDto();
        model.addAttribute("todoForm", todoForm);
        return "addTodo";
    }

    @RequestMapping(value = { "/addTodo" }, method = RequestMethod.POST)
    public String saveTodo(Model model,
                             @ModelAttribute("todoForm") CreateTodoDto todoForm) {
        String title = todoForm.getTitle();
        String description = todoForm.getDescription();
        if ( (title != null && !title.isBlank() &&
                (description !=null && !description.isBlank()))) {
            todoService.createTodo(todoForm);
            return "redirect:/todoList";
        }
        model.addAttribute("errorMessage", errorMessage);
        return "addTodo";
    }
}
