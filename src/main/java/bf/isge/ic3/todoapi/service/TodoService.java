package bf.isge.ic3.todoapi.service;

import bf.isge.ic3.todoapi.dto.CreateTodoDto;
import bf.isge.ic3.todoapi.dto.UpdateTodoDto;
import bf.isge.ic3.todoapi.exception.TodoNotFoundException;
import bf.isge.ic3.todoapi.model.Todo;

import java.util.List;
import java.util.Optional;

public interface TodoService {
    Optional<Todo> findById(int id );
    Todo createTodo(CreateTodoDto todo);
    Todo updateTodo(int id, UpdateTodoDto todo) throws TodoNotFoundException;
    boolean removeTodo(int id);
    List<Todo> findAll();
}
