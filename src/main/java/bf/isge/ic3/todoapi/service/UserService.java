package bf.isge.ic3.todoapi.service;

import bf.isge.ic3.todoapi.model.User;

import java.util.Optional;

public interface UserService {
    Optional<User> findById(int id);
    User addUser(User user);
}
